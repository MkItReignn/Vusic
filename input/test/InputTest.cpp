#include <gtest/gtest.h>

#include "input/Input.h"
#include "dependencies/aubio-master/src/notes/notes.h"
#include "dependencies/aubio-master/src/tempo/tempo.h"
#include "dependencies/aubio-master/src/io/source.h"
#include "dependencies/aubio-master/src/types.h"

using namespace testing;

TEST(InputTest, correctBPM)
{
    Input::Input myInput;
    std::cout << myInput.getString() << std::endl;

    ASSERT_EQ(myInput.getString(), "theString");
}

TEST(AubioTest, settingUp)
{
    uint_t mySamplerate = 44100;
    uint_t myHopSize = 320;
    std::string myAudioSource = "~/home/alex/code/Vusic/musicFiles/MaRLo&Feenixpawl-Lighter_Than_Air.wav";
    auto myAubioSource = new_aubio_source(myAudioSource.data(),
                                          mySamplerate,
                                          myHopSize);
    auto myVec = new_fvec(myHopSize);
    ASSERT_TRUE(myAubioSource);
    ASSERT_TRUE(myVec);
}